// tailwind.config.js
module.exports = {
  theme: {
    extend: {
      colors: {
        'brand-blue': {
          light: '#00a5db',
          default: '#005b99',
          logo: '#0b5d97',
          dark: '#1e437a',
        },
        'danger': '#ab0000'
      }
    }
  }
}
